package tests;

import game.Board;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by oschi on 23.04.16.
 */
public class ThreeInARowTest {

    Board board;

    @Test
    public void threeInARowShouldReturnOne(){
        for(int i = 0; i <= 2; i++){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Reihe 1 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 3; i <= 5; i++){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Reihe 2 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 6; i <= 8; i++){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Reihe 3 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 0; i <= 6; i += 3){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Spalte 1 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 1; i <= 7; i += 3){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Spalte 2 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 2; i <= 8; i += 3){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Spalte 3 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 0; i <= 8; i += 4){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Diagonale 1 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

        for(int i = 2; i <= 6; i += 2){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Diagonale 2 muss 1 returnen", 1, board.threeInARow());
        board.resetBoard();

    }

    @Test
    public void threeInARowShouldReturnMinusOne(){
        for(int i = 0; i <= 2; i++){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Reihe 1 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 3; i <= 5; i++){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Reihe 2 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 6; i <= 8; i++){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Reihe 3 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 0; i <= 6; i += 3){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Spalte 1 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 1; i <= 7; i += 3){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Spalte 2 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 2; i <= 8; i += 3){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Spalte 3 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 0; i <= 8; i += 4){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Diagonale 1 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();

        for(int i = 2; i <= 6; i += 2){
            board.spieler = -board.spieler;
            board.makeMove(i);
        }
        assertEquals("Check Diagonale 2 muss -1 returnen", -1, board.threeInARow());
        board.resetBoard();


    }
}
