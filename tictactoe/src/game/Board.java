package game;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by oschi on 19.04.16.
 */
public class Board {

    /*
        Globale Variablen
     */
    public List<Integer> moves = new ArrayList<>();
    private int[] brett = new int[9];
    public int spieler = 1;
    private int zug = 0;

    @Override
    public String toString(){
        String out = "";
        for(int i = 0; i <= 6; i+=3) {
            System.out.println("test: " + i);
            out += "  " + brett[i] + "  |  " + brett[i+1] + "  |  " + brett[i+2] + "  \n";
            if(i < 6) out += "-----------------\n";
        }
        return out;
    }

    public boolean checkPositionIsFree(int x){
        return (brett[x] == 0);
    }

    public void makeMove(int x){
        brett[x] = spieler;
        System.out.println(moves.size());
        moves.add(x);
        spieler = -spieler;
        zug += 1;
    }

    public void undoMove(){
        if(!moves.isEmpty()) {
            brett[moves.remove(moves.size() - 1)] = 0;
            spieler = -spieler;
        }
    }

    public int threeInARow(){
        int[][] rows = {{0,1,2},{3,4,5},{6,7,8},{0,3,6},{1,4,7},{2,5,8},{0,4,8},{2,4,6}};

        for(int[] row: rows) {
            if (Math.abs(brett[row[0]] + brett[row[1]] + brett[row[2]]) == 3) return brett[row[0]];
        }

        return 0;
    }

    public int listMoves(){
        int sumOfMoves = 0;
        for(int i = 0; i < brett.length; i++){
            if(brett[i] == 0) sumOfMoves = i;
        }
        return sumOfMoves;
    }

    public void resetBoard(){
        for(int i = 0; i <= 8; i++){
            brett[i] = 0;
        }
    }

    public void reset(){
        System.out.println("Spiel wird neugestartet");
        spieler = 1;
        resetBoard();
    }
}
